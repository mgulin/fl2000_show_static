#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  generate_test_images.py
#  
#  Copyright 2016 mgulin
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  



def main(args):
    width = 800
    height = 600
    increment = 8
    rotation = 0
    if(len(args)<5):
        print("No arguments, using default values")
        print("Usage: "+args[0]+" width height increment rotation")
    else:
        width       = int(args[1])
        height      = int(args[2])
        increment   = int(args[3])
        rotation    = int(args[4])
        
    red = 0
    green = 0
    blue = 0
    img = Image.new('RGB', (width, height))
    img2 = Image.new('RGB', (width, height))

    list_of_pixels = list(img.getdata())

    for pixel_num in range(0, len(list_of_pixels)):
        list_of_pixels[pixel_num] = (red, green, blue)
        if(blue>=255):
            if(green>=255):
                if(red>=255):
                    red = 0
                    green = 0
                    blue = 0
                else:
                    red += increment
                    green = 0
                    blue = 0
            else:
                green += increment
                blue=0
        else:
            blue += increment
                
    #print(list_of_pixels)
    img2 = Image.new(img.mode, img.size)
    img2.putdata(list_of_pixels)
    img2 = img2.rotate(rotation)
    filename = 'test_wallpapers/'+str(img2.width)+'x'+str(img2.height)+'_inc'+str(increment)+'_rot'+str(rotation)+'.bmp'
    print('Saving to '+filename)
    img2.save(filename)
    return 0
    
    

if __name__ == '__main__':
    import sys
    from PIL import Image
    sys.exit(main(sys.argv))
