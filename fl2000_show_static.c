/*
 * Experimental program to show "static" output on a a Fresco Logic FL2000
 * All the set-up code is from https://github.com/cy384/fl2000_get_edid. Thank you!
 *
 * Compile with 
 * gcc fl2000_show_static.c -o fl2000_show_static -I/usr/include/libusb-1.0 -lusb-1.0 -Wall
 * 
 * Copyright (C) 2014 cy384
 * Copyright (C) 2016 mgulin
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 *
 * Alternatively, this software may be distributed under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation.  See the LICENSE_GPL file for details.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libusb.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>


static volatile int keepRunning = 1;
static volatile int verbose = 0;

/* for control stuff, we only really work with four bytes at a time, but w/e */
#define RESPONSE_BUFFER_SIZE 512

/* convenience macros */
#define EXPECT_FOUR_BYTES \
if (bytes_back != 4) \
{ \
	fprintf(stderr, "%d bytes from transfer?\n", bytes_back); \
	ret = bytes_back; \
	goto cleanup; \
}

#define PRINT_FOUR_BYTES(wIndex, readwrite) \
printf("%d %c: ", wIndex, readwrite); print_data(data);

#define SILENT_TRANSFER_OUT(wIndex) \
bytes_back = libusb_control_transfer(fl2000_handle, 0x40, 65, 0, wIndex, data, 4, 0); \
EXPECT_FOUR_BYTES;

#define SILENT_TRANSFER_IN(wIndex) \
bytes_back = libusb_control_transfer(fl2000_handle, 0xc0, 64, 0, wIndex, data, 4, 0); \
EXPECT_FOUR_BYTES;

#define SET_DATA(byte0, byte1, byte2, byte3) \
data[0] = byte0; data[1] = byte1; data[2] = byte2; data[3] = byte3;

#define DATA_EQ(byte0, byte1, byte2, byte3) \
(byte0 == data[0] && byte1 == data[1] && byte2 == data[2] && byte3 == data[3])

/* make all transfers silent except in debug mode */
#define TRANSFER_IN(wIndex) SILENT_TRANSFER_IN(wIndex) \
if(verbose){EXPECT_FOUR_BYTES; PRINT_FOUR_BYTES(wIndex, 'R');}

#define TRANSFER_OUT(wIndex) SILENT_TRANSFER_OUT(wIndex) \
if(verbose){EXPECT_FOUR_BYTES; PRINT_FOUR_BYTES(wIndex, 'W');}


#define LOG(message) \
printf("%s\n",message);

#define BULKZERO() \
libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, 500); \

void print_data(uint8_t* data)
{
	int i;
	for (i = 0; i < 4; i++) printf("%.2x ", data[i]);
	printf("\n");
}

void printUsage(char *argv[]) {
	fprintf(stderr, "Usage: %s [-v] [mode]\n", argv[0]);
	fprintf(stderr, "modes: STATIC = 0, LINUS = 1, GRADIENT = 2\n");
}

void intHandler(int dummy) {
    keepRunning = 0;
}

int main(int argc, char *argv[])
{
	int ret = 0;
	int bytes_back = 0;
	uint8_t data[RESPONSE_BUFFER_SIZE] = {0};
	libusb_device_handle* fl2000_handle = 0;
	unsigned char __zero_bin[] = {};
	unsigned int  __zero_bin_len = 0;
	int opt;
	enum {
        STATIC = 0, 
        LINUS = 1, 
        GRADIENTYW = 2, 
        GRADIENTTEST = 3, 
        STATICFULLSCREEN = 4, 
        COMPLEXTOPROW=5, 
        RAWIMAGE=6,
        RAWIMAGE2=7,
        STATIC1080 = 8,
        STATIC800USB3 = 9
    } mode = STATIC;
    static int LAST_MODE_NUMBER = 9;
    static int timeout = 300;
	
	signal(SIGINT, intHandler);
    
    while ((opt = getopt(argc, argv, "v")) != -1) {
        switch (opt) {
        case 'v': verbose = 1; break;
        }
    }
    if(optind>=argc || atoi(argv[optind])<0 || atoi(argv[optind])>LAST_MODE_NUMBER){
		printUsage(argv);
        exit(EXIT_FAILURE);
	}else{
		mode = atoi(argv[optind]);
	}
	
	ret = libusb_init(NULL);
	if (ret < 0) goto cleanup;

	fl2000_handle = libusb_open_device_with_vid_pid(0, 0x1d5c, 0x2000);
	if (!fl2000_handle)
	{
		fprintf(stderr, "couldn't find an fl2000 device\n");
		goto cleanup;
	}

	ret = libusb_set_configuration(fl2000_handle, 1);
	if (ret < 0) goto cleanup;

	ret = libusb_claim_interface(fl2000_handle, 1);
	if (ret < 0) goto cleanup;

	ret = libusb_set_interface_alt_setting(fl2000_handle, 1, 0);
	if (ret < 0) goto cleanup;

	ret = libusb_claim_interface(fl2000_handle, 2);
	if (ret < 0) goto cleanup;

	ret = libusb_set_interface_alt_setting(fl2000_handle, 2, 0);
	if (ret < 0) goto cleanup;

	TRANSFER_IN(32800);

	/* either for priming the interrupt or the EDID stuff? */
	while (!DATA_EQ(0xcc, 0x00, 0x00, 0x8f))
	{
		SET_DATA(0xcc, 0x00, 0x00, 0x10);
		SILENT_TRANSFER_OUT(32800);
		SILENT_TRANSFER_IN(32800);
	}

	/* reset the monitor attachment thing? */
	SET_DATA(0xe1, 0x00, 0x00, 0x00);
	TRANSFER_OUT(32768);
	TRANSFER_IN(32768);

	if (!DATA_EQ(0x00, 0x00, 0x00, 0x00))
	{
		fprintf(stderr, "did you leave your monitor attached?\n");
	}

	/* check for monitor; if none, wait for attach interrupt */
	TRANSFER_IN(32768);
	if (DATA_EQ(0x00, 0x00, 0x00, 0x00))
	{
		bytes_back = 0;
		ret = libusb_interrupt_transfer(fl2000_handle, 0x83, data, 1, &bytes_back, 0);

		if (ret != 0)
		{
			goto cleanup;
		}

		if (bytes_back != 1)
		{
			fprintf(stderr, "%d bytes from interrupt?\n", bytes_back);
		}
	}

	
    /* Let's try some magic control packets which supposedly changes res from 640x480 to 800x600 */
    /* This might set timings and stuff which is different for different monitors? */
    LOG("Start sending magic control packets");
    
	TRANSFER_IN(32840);//15271
	SET_DATA(0x04, 0x80, 0x7d, 0x28);//800x600
	//SET_DATA(0x04, 0x80, 0xfd, 0x7f);//1920x1080
	TRANSFER_OUT(32840);
	
	
	TRANSFER_IN(32828);
	SET_DATA(0x4d, 0x08, 0x01, 0xd4);
	TRANSFER_OUT(32828);
	
    //input not support! so this is definately resolution
	SET_DATA(0x10, 0x62, 0x7f, 0x00);//800x600
	//SET_DATA(0x06, 0x61, 0x59, 0x00);//1920x1080
	TRANSFER_OUT(32812);
	
	TRANSFER_IN(32840);
	SET_DATA(0x04, 0x80, 0x7d, 0x28);//800x600
	//SET_DATA(0x04, 0x80, 0xfd, 0x7f);//1920x1080
	TRANSFER_OUT(32840);
	TRANSFER_IN(32812);
	
    TRANSFER_IN(32828);
	SET_DATA(0x4d, 0x08, 0x01, 0xc4);
	TRANSFER_OUT(32828);//15289
	
    
    
    //makes 800px image only on top?
	TRANSFER_IN(32828);	
    SET_DATA(0x4d, 0x08, 0x01, 0xd4);//800x600
	//SET_DATA(0x4d, 0x08, 0x01, 0xe5);//1920x1080
	TRANSFER_OUT(32828);
    
    
	TRANSFER_IN(32772);//15295
	SET_DATA(0xe1, 0x00, 0x00, 0x00);//800x600
	//SET_DATA(0x1c, 0x03, 0x10, 0x00);//1920x1080
	TRANSFER_OUT(32772);
    
	TRANSFER_IN(32772);
	SET_DATA(0x10, 0x03, 0x10, 0x00);
	TRANSFER_OUT(32772);//15297
    
    //draws 800px half screen?
	TRANSFER_IN(32772);
	SET_DATA(0x9d, 0x03, 0x10, 0x81);//800x600
	//SET_DATA(0x9d, 0x03, 0x10, 0x00);//1920x1080
	TRANSFER_OUT(32772);//15301
	
    //input not support! so this is definately resolution
	SET_DATA(0x20, 0x04, 0x20, 0x03);//800x600
	//SET_DATA(0x98, 0x08, 0x80, 0x07);//1920x1080
	TRANSFER_OUT(32776);
    TRANSFER_IN(32776);
    
	SET_DATA(0xd9, 0x00, 0x80, 0x00);//800x600
	//SET_DATA(0xc1, 0x00, 0x2c, 0x00);//1920x1080
	TRANSFER_OUT(32780);
	TRANSFER_IN(32780);
    
	//input not support! so this is definately resolution
    //slow transfer?
	SET_DATA(0x74, 0x02, 0x58, 0x02);//800x600
	//SET_DATA(0x65, 0x04, 0x38, 0x04);//1920x1080
	TRANSFER_OUT(32784);
	TRANSFER_IN(32784);
	
    //flickers, but whole image. timings?
    //slow transfer
	SET_DATA(0x1c, 0x00, 0xc4, 0x01);//800x600
	//SET_DATA(0x2a, 0x00, 0xa5, 0x02);//1920x1080
	TRANSFER_OUT(32788);
	TRANSFER_IN(32788);
	TRANSFER_IN(32796);
	
	SET_DATA(0x00, 0x00, 0x00, 0x00);
	TRANSFER_OUT(32796);
	TRANSFER_IN(112);
	
	SET_DATA(0x85, 0x40, 0x18, 0x04);
	TRANSFER_OUT(112);
	
	LOG("End sending magic control packets");
	
	/* At this point the display is "awake" and stays on after application has finished */
	

	
	/* After the control packets we still need to send some "smaller" packets */
	
	LOG("Start sending magic after_control packets");
    //800x600 start
	#include "after_control/15327.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15327_bin, __15327_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15328.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15328_bin, __15328_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15329.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15329_bin, __15329_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15335.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15335_bin, __15335_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15336.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15336_bin, __15336_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15337.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15337_bin, __15337_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15343.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15343_bin, __15343_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15344.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15344_bin, __15344_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15345.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15345_bin, __15345_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15351.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15351_bin, __15351_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15352.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15352_bin, __15352_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15353.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15353_bin, __15353_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15359.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15359_bin, __15359_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15360.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15360_bin, __15360_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15361.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15361_bin, __15361_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15367.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15367_bin, __15367_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15368.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15368_bin, __15368_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	#include "after_control/15369.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __15369_bin, __15369_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	
	libusb_bulk_transfer(fl2000_handle, 0x01, __zero_bin, __zero_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	//800x600 end
    
    //1920x1080 start
    /*
    #include "1920x1080/after_control/39066.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __39066_bin, __39066_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
    BULKZERO();
    
    #include "1920x1080/after_control/39068.bin.c"
	libusb_bulk_transfer(fl2000_handle, 0x01, __39068_bin, __39068_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	BULKZERO();
    
    
	libusb_bulk_transfer(fl2000_handle, 0x01, __39068_bin, __39068_bin_len, &bytes_back, timeout);
	fprintf(stderr, "%d bytes from bulk\n", bytes_back);if(!keepRunning)goto cleanup;
	BULKZERO();
    */
    //1920x1080 end
    
	LOG("End sending magic after_control packets");
	
	
	/* From this point on we just need to send "frames" */
	LOG("Start sending actual frame packets");
	if(mode==STATIC){
		LOG("Showing static image");
		while(keepRunning){
			#include "dot-packets/800x600_white_FF0000_dot_in_0_2.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __800x600_white_FF0000_dot_in_0_2_bin, __800x600_white_FF0000_dot_in_0_2_bin_len, &bytes_back, timeout);
			BULKZERO();
		}
	}
	if(mode==LINUS){
        LOG("Showing our favorite fennoswede");
        while(keepRunning){
            
            #include "linus2/1288.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1288_bin, __1288_bin_len, &bytes_back, timeout);

            #include "linus2/1289.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1289_bin, __1289_bin_len, &bytes_back, timeout);

            #include "linus2/1290.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1290_bin, __1290_bin_len, &bytes_back, timeout);

            #include "linus2/1291.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1291_bin, __1291_bin_len, &bytes_back, timeout);

            #include "linus2/1292.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1292_bin, __1292_bin_len, &bytes_back, timeout);

            #include "linus2/1293.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1293_bin, __1293_bin_len, &bytes_back, timeout);

            #include "linus2/1294.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1294_bin, __1294_bin_len, &bytes_back, timeout);

            #include "linus2/1295.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1295_bin, __1295_bin_len, &bytes_back, timeout);

            #include "linus2/1296.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1296_bin, __1296_bin_len, &bytes_back, timeout);

            #include "linus2/1297.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1297_bin, __1297_bin_len, &bytes_back, timeout);

            #include "linus2/1298.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1298_bin, __1298_bin_len, &bytes_back, timeout);

            #include "linus2/1299.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1299_bin, __1299_bin_len, &bytes_back, timeout);

            #include "linus2/1300.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1300_bin, __1300_bin_len, &bytes_back, timeout);

            #include "linus2/1301.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1301_bin, __1301_bin_len, &bytes_back, timeout);

            #include "linus2/1302.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1302_bin, __1302_bin_len, &bytes_back, timeout);

            #include "linus2/1303.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1303_bin, __1303_bin_len, &bytes_back, timeout);

            #include "linus2/1305.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1305_bin, __1305_bin_len, &bytes_back, timeout);

            #include "linus2/1307.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1307_bin, __1307_bin_len, &bytes_back, timeout);

            #include "linus2/1309.bin.c"
            libusb_bulk_transfer(fl2000_handle, 0x01, __1309_bin, __1309_bin_len, &bytes_back, timeout);

            BULKZERO();
        }
	}
	if(mode==GRADIENTYW){
		LOG("Starting gradient yellow to white mode");
		unsigned char __800x600_fullscreen_color[] = {
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xfe, 0x52,
		  0x01, 0xff, 0x80, 0x80
		};
		unsigned int __800x600_fullscreen_color_len = 64;
		
		unsigned char currentColor = 0x01;
		int i = 0;
		while(keepRunning){
			printf("Switching to color: %#2x (%u)\n", currentColor, currentColor);
			i = 0;
			while(keepRunning && i<10){
				libusb_bulk_transfer(fl2000_handle, 0x01, __800x600_fullscreen_color, __800x600_fullscreen_color_len, &bytes_back, 1000);
				BULKZERO();
				
				i++;
			}
			currentColor++;
			for(int j=0;j<=__800x600_fullscreen_color_len;j+=4){
				__800x600_fullscreen_color[j] = currentColor;
				//__800x600_fullscreen_color[j+1]  = currentColor;
			}
		}
	}
	
	if(mode==GRADIENTTEST){
		LOG("Showing test mode gradient. This might or might not show anything since I change it all the time.");
		unsigned char __800x600_fullscreen_color[] = {
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xfe, 0x52,
		  0x01, 0xff, 0x80, 0x80
		};
		unsigned int __800x600_fullscreen_color_len = 64;
		
		unsigned char currentColorA = 0x00;
		unsigned char currentColorB = 0x80;
		int i = 0;
		while(keepRunning){
			printf("Switching to color: %#2x (%u),%#2x (%u)\n", currentColorA, currentColorA, currentColorB, currentColorB);
			i = 0;
			while(keepRunning && i<10){
				libusb_bulk_transfer(fl2000_handle, 0x01, __800x600_fullscreen_color, __800x600_fullscreen_color_len, &bytes_back, 1000);
				BULKZERO();
				i++;
			}
			currentColorA++;
			for(int j=0;j<=__800x600_fullscreen_color_len;j+=2){
				__800x600_fullscreen_color[j] = currentColorA;
				__800x600_fullscreen_color[j+1]  = currentColorB;
			}
            if(currentColorA==0xff){
                currentColorB++;
                currentColorA=0;
            }
		}
	}
	
	if(mode==STATICFULLSCREEN){
		LOG("Showing test mode static. This might or might not show anything since I change it all the time.");
		/*unsigned char __800x600_fullscreen_color[] = {
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xfe, 0x52,
		  0x01, 0xff, 0x80, 0x80
		};*/
		unsigned char __800x600_fullscreen_color[] = {
		  0x01, 0xff, 0xff, 0x00, 0x01, 0xff, 0xff, 0x00, 0x01, 0xff, 0xff, 0x52,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x7f, 0xc0, 0xff, 0xff, 0x52,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0x00, 0x00, 0xFF, 0xc0, 0xf0, 0xf0, 0x7f,
		  0x01, 0xff, 0xff, 0x52, 0x01, 0xff, 0xff, 0x7f, 0xc0, 0xff, 0xff, 0x52,
		  0x01, 0xff, 0xff, 0x7f, 0x01, 0xff, 0xff, 0x00, 0x01, 0xff, 0xfe, 0x52,
		  0xc0, 0xff, 0x80, 0x80
		};
		unsigned int __800x600_fullscreen_color_len = 64;
		
		unsigned char currentColor = 0xFF;
		for(int j=0;j<=__800x600_fullscreen_color_len;j+=4){
				__800x600_fullscreen_color[j] = 0xC0;
				__800x600_fullscreen_color[j+1]  = currentColor;
		}
			
		while(keepRunning){
			printf("Switching to color: %#2x (%u)\n", currentColor, currentColor);

			while(keepRunning){
				libusb_bulk_transfer(fl2000_handle, 0x01, __800x600_fullscreen_color, __800x600_fullscreen_color_len, &bytes_back, 1000);
				BULKZERO();
			}
			
		}
	}
    if(mode==COMPLEXTOPROW){
        LOG("Showing complex top row");
		while(keepRunning){
			#include "complex_top_row/141.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __141_bin, __141_bin_len, &bytes_back, timeout);
			BULKZERO();
		}
    }
    if(mode==RAWIMAGE){
        LOG("Showing raw generated image");
        //so we first have a buffer of size 800x600 * 2 =960000 bytes
        unsigned char *whole_image;
        whole_image = (unsigned char *)malloc(960000);
        memset(whole_image,0xff,960000);
        
        //Manipulate image buffer
        unsigned char min_a=0x00;
        unsigned char max_a=0xff;
        unsigned char min_b=0x80;
        unsigned char max_b=0xff;
        
        unsigned char a=min_a;
        unsigned char b=min_b;
        for(int i=0;i<960000;i+=2){
            
            //whole_image[i] = a;
            //whole_image[i+1] = b;
            
            //whole_image[i] = 0xFF;
            //whole_image[i+1] = 0b11100000;
            
            // still white
            whole_image[i]   = 0b01111111; 
            whole_image[i+1] = 0b11111111; 
            
            // a tad yellow
            whole_image[i]   = 0b00001111; 
            whole_image[i+1] = 0b11111111; 
            
            // really yellow
            whole_image[i]   = 0b00000000; 
            whole_image[i+1] = 0b11111111; 
            
            // green
            whole_image[i]   = 0b11000000;
            whole_image[i+1] = 0b10011111; 
            
            // light blue
            //whole_image[i]   = 0b11111100; 
            //whole_image[i+1] = 0b10011111; 
            
            //dark green
            whole_image[i]   = 0b11000000;
            whole_image[i+1] = 0b10001100; 
            
            //brown
            whole_image[i]   = 0b11000000;
            whole_image[i+1] = 0b10111000; 
            
            //red
            whole_image[i]   = 0b00000000;
            whole_image[i+1] = 0b10111100; //dark red
            whole_image[i+1] = 0b11000000; 
            whole_image[i+1] = 0b11100100; 
            whole_image[i+1] = 0b11101000;//normal red 
            whole_image[i+1] = 0b11101100; 
            whole_image[i+1] = 0b11000000; 
            
            //blue
            whole_image[i]   = 0b11111111;
            whole_image[i+1] = 0b10001100; 
            whole_image[i+1] = 0b10010000; 
            whole_image[i+1] = 0b10010100; 
            whole_image[i+1] = 0b10011000; 
            whole_image[i+1] = 0b10011100; 
            

            
            if(a==max_a){
                if(b==max_b){
                    //break;
                    a=min_a;
                    b=min_b;
                }else{
                    b++;
                }
            }else{
                if(b==max_b){
                    a++;
                    b=min_b;
                }else{
                    b++;
                }
            }
        }
        
        
        //We split this into packets with maxsize 20480
        //This gives us 46 whole packets and one smaller
        //End with "empty" packet
        
		unsigned int packet_index = 0;
        while(keepRunning){
            
            if(packet_index <= 45){
                libusb_bulk_transfer(fl2000_handle, 0x01, (whole_image+(20480*packet_index)), 20480, &bytes_back, timeout);
                packet_index++;
            }else{ 
                if(packet_index == 46){
                    libusb_bulk_transfer(fl2000_handle, 0x01, (whole_image+(20480*packet_index)), 17920, &bytes_back, timeout);
                    packet_index++;
                }else{
                    BULKZERO();
                    packet_index = 0;
                }
            }
            
		}
    }
    if(mode==RAWIMAGE2){
        LOG("Showing raw generated image");
        //so we first have a buffer of size 800x600 * 2 =960000 bytes
        unsigned char *whole_image;
        whole_image = (unsigned char *)malloc(960000);
        memset(whole_image,0xff,960000);
        
        //Manipulate image buffer
        unsigned char min_a=0b00001000; //1= 2= 3= 4= 5=grönblålila 6= 7= 8=
        unsigned char max_a=0xff;
        unsigned char min_b=0x80;
        unsigned char max_b=0xff;
        
        unsigned char a=min_a;
        unsigned char b=min_b;
        int i=0;
        while(keepRunning && a <= 255 && b <= 255){
            printf("Switching to color: %#2x (%u) , %#2x (%u)\n", a, a, b, b);
            i=0;
            for(int j=0;j<960000;j+=2){
                
                whole_image[j] = a;
                whole_image[j+1] = b;

            }
            
            
            //We split this into packets with maxsize 20480
            //This gives us 46 whole packets and one smaller
            //End with "empty" packet
            
            unsigned int packet_index = 0;
            while(keepRunning && i < 20){
                
                if(packet_index <= 45){
                    libusb_bulk_transfer(fl2000_handle, 0x01, (whole_image+(20480*packet_index)), 20480, &bytes_back, timeout);
                    packet_index++;
                }else{ 
                    if(packet_index == 46){
                        libusb_bulk_transfer(fl2000_handle, 0x01, (whole_image+(20480*packet_index)), 17920, &bytes_back, timeout);
                        packet_index++;
                    }else{
                        BULKZERO();
                        packet_index = 0;
                        i++;
                    }
                }
                
            }
            
            if(a==max_a){
                if(b==max_b){
                    //break;
                    a=min_a;
                    b=min_b;
                }else{
                    b++;
                }
            }else{
                if(b==max_b){
                    a++;
                    b=min_b;
                }else{
                    b++;
                }
            }
        }
    }
    if(mode==STATIC1080){
		LOG("Showing static image");
		while(keepRunning){
			#include "1920x1080/39076.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39076_bin, __39076_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39078.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39078_bin, __39078_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39082.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39082_bin, __39082_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39086.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39086_bin, __39086_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39090.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39090_bin, __39090_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39094.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39094_bin, __39094_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/39098.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __39098_bin, __39098_bin_len, &bytes_back, timeout);
			BULKZERO();
            /*
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            
            #include "1920x1080/390.bin.c"
			libusb_bulk_transfer(fl2000_handle, 0x01, __390_bin, __390_bin_len, &bytes_back, timeout);
			BULKZERO();
            */
            
		}
	}
    if(mode==STATIC800USB3){
        #include "usb3_800x600/3050.bin.c"
        while(keepRunning){
            
            libusb_bulk_transfer(fl2000_handle, 0x01, __3050_bin, __3050_bin_len, &bytes_back, timeout);
            //usleep(1);
            BULKZERO();
            //usleep(1);
        }
    }


	/* clean up before exiting */
	cleanup:
    
    /* Shut off monitor/device */
    TRANSFER_IN(32828);
    SET_DATA(0x4d,0x08,0x01,0xd4);
    TRANSFER_OUT(32828);

    TRANSFER_IN(32840);
    SET_DATA(0x4d,0x80,0xbc,0x0b);
    TRANSFER_OUT(32840);

	fprintf(stdout, "\nCleaning up...\n");
	/* is there a way to check for open interfaces? w/e */	
	if (fl2000_handle)
	{
		libusb_release_interface(fl2000_handle, 1);
		libusb_release_interface(fl2000_handle, 2);
		/* do we need to set configuration back to 0? not really */
		libusb_set_configuration(fl2000_handle, 0);
		libusb_close(fl2000_handle);
	}else{
		fprintf(stderr, "fl2000_handle not found!\n");
	}

	libusb_exit(NULL);
	if (ret < 0) fprintf(stderr, "%s (%d)\n", libusb_error_name(ret), ret);
	return ret;
}

